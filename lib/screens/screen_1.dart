import 'package:flutter/material.dart';

class ScreenOnePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: screenHeight / 2,
              // child: Image.asset("assets/images/man_trecking.jpg"),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(40),
                    bottomRight: Radius.circular(40),
                  ),
                  image: DecorationImage(
                      image: AssetImage("assets/images/man_trecking.jpg"),
                      fit: BoxFit.cover)),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 5),
              child: Text("Winter",style: TextStyle(fontSize: 40,fontWeight: FontWeight.w700),),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
              child: Text("Vacation Trips",style: TextStyle(fontSize: 40,fontWeight: FontWeight.w700),),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
              child: Text("Enjoy your winter vacations with warmth",style: TextStyle(fontSize: 18,color: Colors.grey),),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(20, 2, 20, 0),
              child: Text("and amaizing sightseeing on the mountains.",style: TextStyle(fontSize: 18,color: Colors.grey),),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(20, 2, 20, 0),
              child: Text("Enjoy the best experience with us!",style: TextStyle(fontSize: 18,color: Colors.grey),),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
              padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Colors.purpleAccent,
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Let's Go!",style: TextStyle(fontSize: 20,color: Colors.white),),
                  SizedBox(width: 10,),
                  Image.asset("assets/images/right-arrow.png",width: 20,color: Colors.white,)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
