import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';

class ScreenTwoPage extends StatefulWidget {
  @override
  State<ScreenTwoPage> createState() => _ScreenTwoPageState();
}

class _ScreenTwoPageState extends State<ScreenTwoPage> {
  PageController pagecontroller = PageController(viewportFraction: 0.85);
  var _currpagevalue = 0.0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pagecontroller.addListener(() {
      setState(() {
        _currpagevalue = pagecontroller.page!;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    bool isClicked = true;
    Color color = Colors.purple;
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Image.asset(
                          "assets/images/hamburger.png",
                          width: 25,
                          color: Colors.black,
                        ),
                        Text(
                          "Discover",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 30),
                        ),
                        Stack(children: [
                          CircleAvatar(
                            backgroundColor: Color(0xff00A3FF),
                            backgroundImage:
                                AssetImage("assets/images/man_trecking.jpg"),
                            radius: 20.0,
                          ),
                          Positioned(
                              left: 0,
                              bottom: 0,
                              child: Container(
                                  padding: EdgeInsets.all(7.5),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 2, color: Colors.white),
                                      borderRadius: BorderRadius.circular(90.0),
                                      color: Colors.red)))
                        ])
                      ],
                    ),
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          getCustomizedText("Popular  ", color: Colors.purple),
                          getCustomizedText("Featured  "),
                          getCustomizedText("Most Visited  "),
                          getCustomizedText("Europe  "),
                          getCustomizedText("Asia  "),
                          getCustomizedText("America ")
                        ],
                      ),
                    ),
                  ),
                  Container(
                    // margin: EdgeInsets.all(20),
                    height: screenHeight / 3,
                    child: PageView.builder(
                      controller: pagecontroller,
                      itemCount: 5,
                      itemBuilder: (context, index) {
                        return Stack(
                          children: [
                            Container(
                              margin: EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color:
                                    index.isEven ? Colors.blue : Colors.green,
                                image: DecorationImage(
                                    image: AssetImage(
                                      "assets/images/mountain/mountain${index}.jpeg",
                                    ),
                                    fit: BoxFit.cover),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                margin: EdgeInsets.all(8),
                                height: screenHeight / 7,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Colors.black.withOpacity(0.3),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Northern Mountain",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 25),
                                        ),
                                        CircleAvatar(
                                          backgroundColor: Colors.white,
                                          child: Image.asset(
                                            "assets/images/heart.png",
                                            height: 25,
                                            color: Colors.pinkAccent,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Wrap(
                                          children: List.generate(5, (index) {
                                            return Icon(
                                              Icons.star,
                                              color: Colors.yellowAccent,
                                              size: 20,
                                            );
                                          }),
                                        ),
                                        Text(
                                          "4.5",
                                          style: TextStyle(color: Colors.white),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                  DotsIndicator(
                    dotsCount: 5,
                    position: _currpagevalue,
                    decorator: DotsDecorator(
                      size: const Size.square(9.0),
                      activeSize: const Size(18.0, 9.0),
                      activeShape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Recommended",
                          style: TextStyle(
                              fontSize: 25, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "View All",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      getCustomizedCard(screenHeight, screenWidth, 0),
                      getCustomizedCard(screenHeight, screenWidth, 1),
                    ],
                  ),
                  Row(
                    children: [
                      getCustomizedCard(screenHeight, screenWidth, 2),
                      getCustomizedCard(screenHeight, screenWidth, 3),
                    ],
                  ),
                ],
              ),
            ),
            Positioned(
              left: 10,
              right: 10,
              bottom: 10,
              child: Container(
                height: screenHeight / 8,
                width: screenWidth,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  color: Colors.white,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Icon(Icons.home,size: 35,color: Colors.purple,),
                        Text("___")
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Icon(Icons.event_note_sharp,size: 30,color: Colors.grey.withOpacity(0.5)),
                        Text("___",style: TextStyle(color: Colors.white))
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Icon(Icons.search,size: 30,color: Colors.grey.withOpacity(0.5)),
                        Text("___",style: TextStyle(color: Colors.white))
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Icon(Icons.person_rounded,size: 30,color: Colors.grey.withOpacity(0.5)),
                        Text("___",style: TextStyle(color: Colors.white),)
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget getCustomizedText(text, {Color? color, size}) {
    return Text(
      text,
      style: TextStyle(color: color ?? Colors.black, fontSize: size ?? 18),
    );
  }

  Widget getCustomizedCard(screenHeight, screenWidth, int index) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.all(15),
        height: screenHeight / 4,
        child: Stack(
          fit: StackFit.expand,
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Colors.red,
                image: DecorationImage(
                    image: AssetImage(
                        "assets/images/mountain/mountain${index}.jpeg"),
                    fit: BoxFit.cover),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: screenHeight / 7,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.black.withOpacity(0.2),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Northern Mountain",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                      Row(
                        children: [
                          Wrap(
                            children: List.generate(5, (index) {
                              return Icon(
                                Icons.star,
                                color: Colors.yellowAccent,
                                size: 20,
                              );
                            }),
                          ),
                          Text(
                            "4.5",
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
                right: screenWidth / 20,
                bottom: screenHeight / 8,
                child: CircleAvatar(
                  radius: 15,
                  backgroundColor: Colors.white,
                  child: Image.asset(
                    "assets/images/heart.png",
                    height: 15,
                    color: Colors.pinkAccent,
                  ),
                ))
          ],
        ),
      ),
    );
  }

  // Widget getIcons(IconData icon, {Color color = Colors.grey, size = 20.0}) {
  //   return Icon(
  //     icon,
  //     size: size,
  //     color: color,
  //   );
  // }
}
