import 'package:flutter/material.dart';
import 'package:screen_practice/screens/dimensions.dart';

class ScreenThreePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("screenHeight:${Dimensions.screenHeight}");
    print("screenWidth:${Dimensions.screenWidth}");
    var screenWidth = MediaQuery.of(context).size.width;
    // TODO: implement build
    return Scaffold(
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: [
            Image.asset(
              "assets/images/mountain/mountain4.jpeg",
              fit: BoxFit.cover,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(
                      left: Dimensions.width10, top: Dimensions.height10),
                  child: Image.asset(
                    "assets/images/menu.png",
                    width: Dimensions.width25,
                    color: Colors.black,
                  ),
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      padding: EdgeInsets.only(
                          left: Dimensions.width15, top: Dimensions.height15),
                      height: Dimensions.height350,
                      width: screenWidth,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(Dimensions.radius30),
                            topRight: Radius.circular(Dimensions.radius30),
                          ),
                          color: Colors.white),
                      child: Dimensions.screenWidth >= 300
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Mount Fuji",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: Dimensions.font30),
                                ),
                                Row(
                                  children: [
                                    Icon(Icons.location_on),
                                    Text(
                                      "Honshu,Japan",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Wrap(
                                      children: List.generate(
                                        5,
                                        (index) => Icon(
                                          Icons.star,
                                          size: Dimensions.iconsize24,
                                          color: Colors.yellow,
                                        ),
                                      ),
                                    ),
                                    Text(
                                      "4.9",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: Dimensions.height10,
                                ),
                                Container(
                                  width: Dimensions.width250,
                                  // color: Colors.black,
                                  child: Row(
                                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    // mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Container(
                                        height: Dimensions.height50,
                                        width: Dimensions.width30,
                                        decoration: BoxDecoration(
                                            color: Colors.blue,
                                            borderRadius: BorderRadius.circular(
                                                Dimensions.radius15)),
                                        child: Center(
                                            child: Text(
                                          "-",
                                          style: TextStyle(
                                              fontSize: Dimensions.font25,
                                              color: Colors.white),
                                        )),
                                      ),
                                      Container(
                                        height: Dimensions.height40,
                                        width: Dimensions.width60,
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.grey.withOpacity(0.2)),
                                        child: Center(
                                          child: Text(
                                            "5",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        height: Dimensions.height50,
                                        width: Dimensions.width30,
                                        decoration: BoxDecoration(
                                            color: Colors.blue,
                                            borderRadius: BorderRadius.circular(
                                                Dimensions.radius15)),
                                        child: Center(
                                            child: Text(
                                          "+",
                                          style: TextStyle(
                                              fontSize: Dimensions.font25,
                                              color: Colors.white),
                                        )),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            left: Dimensions.width10),
                                        child: Icon(Icons.access_time),
                                      ),
                                      Text("5 days")
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: Dimensions.height10,
                                ),
                                Text(
                                  "Description",
                                  style: TextStyle(
                                      fontSize: Dimensions.font25,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: Dimensions.height10,
                                ),
                                Text(
                                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."),
                                SizedBox(
                                  height: Dimensions.height10,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          "\$400",
                                          style: TextStyle(
                                              fontSize: Dimensions.font30,
                                              color: Colors.purple),
                                        ),
                                        Text(
                                          "/Package",
                                          style: TextStyle(
                                              fontSize: Dimensions.font20,
                                              color: Colors.purple),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          right: Dimensions.width10),
                                      height: Dimensions.height40,
                                      width: Dimensions.width120,
                                      decoration: BoxDecoration(
                                          color: Colors.purple,
                                          borderRadius: BorderRadius.circular(
                                              Dimensions.radius30)),
                                      child: Center(
                                        child: Text(
                                          "Book Now",
                                          style: TextStyle(
                                              fontSize: Dimensions.font18,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            )
                          : SingleChildScrollView(
                              child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Mount Fuji",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: Dimensions.font30),
                                ),
                                Row(
                                  children: [
                                    Icon(Icons.location_on),
                                    Text(
                                      "Honshu,Japan",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Wrap(
                                      children: List.generate(
                                        5,
                                        (index) => Icon(
                                          Icons.star,
                                          size: Dimensions.iconsize24,
                                          color: Colors.yellow,
                                        ),
                                      ),
                                    ),
                                    Text(
                                      "4.9",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: Dimensions.height10,
                                ),
                                Container(
                                  width: Dimensions.width250,
                                  // color: Colors.black,
                                  child: Row(
                                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    // mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Container(
                                        height: Dimensions.height50,
                                        width: Dimensions.width30,
                                        decoration: BoxDecoration(
                                            color: Colors.blue,
                                            borderRadius: BorderRadius.circular(
                                                Dimensions.radius15)),
                                        child: Center(
                                            child: Text(
                                          "-",
                                          style: TextStyle(
                                              fontSize: Dimensions.font25,
                                              color: Colors.white),
                                        )),
                                      ),
                                      Container(
                                        height: Dimensions.height40,
                                        width: Dimensions.width60,
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.grey.withOpacity(0.2)),
                                        child: Center(
                                          child: Text(
                                            "5",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        height: Dimensions.height50,
                                        width: Dimensions.width30,
                                        decoration: BoxDecoration(
                                            color: Colors.blue,
                                            borderRadius: BorderRadius.circular(
                                                Dimensions.radius15)),
                                        child: Center(
                                            child: Text(
                                          "+",
                                          style: TextStyle(
                                              fontSize: Dimensions.font25,
                                              color: Colors.white),
                                        )),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            left: Dimensions.width10),
                                        child: Icon(Icons.access_time),
                                      ),
                                      Text("5 days")
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: Dimensions.height10,
                                ),
                                Text(
                                  "Description",
                                  style: TextStyle(
                                      fontSize: Dimensions.font25,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: Dimensions.height10,
                                ),
                                Text(
                                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."),
                                SizedBox(
                                  height: Dimensions.height10,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          "\$400",
                                          style: TextStyle(
                                              fontSize: Dimensions.font30,
                                              color: Colors.purple),
                                        ),
                                        Text(
                                          "/Package",
                                          style: TextStyle(
                                              fontSize: Dimensions.font20,
                                              color: Colors.purple),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          right: Dimensions.width10),
                                      height: Dimensions.height40,
                                      width: Dimensions.width120,
                                      decoration: BoxDecoration(
                                          color: Colors.purple,
                                          borderRadius: BorderRadius.circular(
                                              Dimensions.radius30)),
                                      child: Center(
                                        child: Text(
                                          "Book Now",
                                          style: TextStyle(
                                              fontSize: Dimensions.font18,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            )),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
